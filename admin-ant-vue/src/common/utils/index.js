

import objUtils from './objUtils'
import crypto from './crypto'
import Vue from 'vue'
let install={
    install(Vue,options) {
        Vue.prototype.$objUtils= objUtils;
        Vue.prototype.$crypto=crypto;
    }
}
Vue.use(install);