export default {
    tableToObject(data) {
        let datas = data.split("_");
        if (datas.length > 0) {
            datas.forEach((item, index) => {
                if (index !== 0) {
                    let start = item.substring(0, 1);
                    let end = item.substring(1);
                    datas[index] = start.toLocaleUpperCase() + end;
                }
            });
            let res = "";
            datas.forEach((item, index) => {
                res += item;
            });
            return res;
        }

    },
    objectTotable(data) {
        let reg = new RegExp("[A-Z]", "g");

        let left = "";
        let right = "";
        let newData = data;
        let offset = 0;
        let res = [];
        while (res) {
            res = reg.exec(data);
            if (!res) {
                break;
            }
            let item = res[0];
            let pos = res.index + offset;
            left = newData.substring(0, pos);
            right = newData.substring(pos + 1, newData.length);
            item = "_" + item.toLocaleLowerCase()
            newData = left + item + right;
        }

        return newData;

    },
    convertObj(target,obj){
        if(typeof target==='object' && typeof obj==='object'){
            Object.keys(obj).forEach(item=>{
                if(target[item]===null||target[item]===undefined){
                    target[item]=obj[item]
                }
            });
        }else {
            return {};
        }
    },
    convertObjAll(target,obj){
        if(typeof target==='object' && typeof obj==='object'){
            Object.keys(obj).forEach(item=>{
                    target[item]=obj[item]
            });
        }else {
            return {};
        }
    },
    initObjDefault(target,def){
        if(typeof target==='object' && typeof def==='object'){
            Object.keys(target).forEach(item=>{
                if(def[item]!==undefined){
                    target[item]=def[item]
                }else {
                    target[item]=null;
                }
            })
        }else {
            return {};
        }
    }
}

