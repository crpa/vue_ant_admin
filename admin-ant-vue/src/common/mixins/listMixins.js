export default {
  data() {
    return {
      selectedRowKeys: [],
      selectedRows: [],
      rowSelection: {
        fixed: true,
        onChange: this.onSelectChange
      },
      tableData: [],
      loading: false,
    }
  },
  created() {
    if (!this.noCurdInit) {
      this.initList()
    }

  },
  methods: {
    initList() {
      if (this.preList) {
        this.preList()
      }
      if (this.customerInit) {
        this.customerInit()
      }
      this.list()
    },
    onSelectChange(selectedRowKeys, selectedRows) {
      this.selectedRowKeys = selectedRowKeys
      this.selectedRows = selectedRows
    },
    reset() {
      this.searchParam = {}
      this.list()
    },
    refresh() {
      if (this.customerInit) {
        this.customerInit()
      }
      this.list()
    },
    list() {
      if (!this.getList) {
        return
      }
      this.loading = true
      this.$nextTick(() => {
        this.getList().then((res) => {
          if (res.status === 0) {
            this.tableData = res.data
          } else {
            this.$message.error(res.msg)
          }
          if (this.listBack) {
            this.listBack()
          }
          this.loading = false
        }).catch(res => {
          this.loading = false
        })
      })
    },

    add() {
      this.$refs.save.setData()
    },

    handleEdit(data) {
      this.$refs.save.edit({ ...data })
    },
    handleDelete(data) {
      if (this.delete) {
        this.delete(data).then(res => {
          if (res.status === 0) {
            this.$message.success('删除成功')
            this.refresh()
          } else {
            this.$message.error(res.msg)
          }
        })
      }
    },
    handleMenuClick(e) {
      if (!this.multiOperator) {
        return
      }
      let operator = this.multiOperator.find(item => item.key === e.key)
      if (!operator || !operator.method) {
        return
      }

      let ids = this.selectedRowKeys.join(',')
      this.$confirm({
        title: operator.tip || '确定批量操作吗？',
        okText: '确定',
        okType: 'primary',
        cancelText: '取消',
        onOk: () => {

          operator.method({ ids: ids }).then(res => {
            if (res.status === 0) {
              this.$message.success(operator.msg || '批量操作成功!')
              this.refresh()
            } else {
              this.$message.error(res.msg)
            }
          })
        },
        onCancel() {
        }
      })

    }


  }
}
