export default {
  data() {
    return {
      title: '',
      editForm: {},
      formLabelWidth: '120px',
      visible: false,
      loading: false,
      fullscreen: false,
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 13 }
      },
      form: this.$form.createForm(this),
    }
  },
  created() {

  },
  methods: {

    add(){
      if (this.defaultVal) {
        this.editForm = { ...this.editForm, ...this.defaultVal }
      } else {
        this.editForm = {}
      }
      this.title = '新增'
      this.$nextTick(()=>{
        this.visible = true
        if (this.setDataBack) {
          this.setDataBack(data)
        }
      })

    },
    edit(data){

      this.editForm = { ...this.editForm,...data }
      if (this.defaultVal) {
        this.editForm = { ...this.editForm, ...this.defaultVal }
      }
      this.title = '编辑'


      this.$nextTick(()=>{
        this.visible = true
        if (this.setDataBack) {
          this.setDataBack(data)
        }
      })

    },
    handleSubmit() {
      const { form: { validateFields } } = this
      validateFields((err, values) => {
        if (!err) {

          values={...this.editForm,...values}
          if(this.filterForm){
            values=this.filterForm(values);
          }
          if(this.commit){
            this.loading = true
            this.commit(values).then(res=> {
              if (res.status === 0) {
                this.$message.success("保存成功")
                this.$emit('refresh')
                this.visible = false
              } else {
                this.$message.error(res.msg)
              }
              this.loading = false

            }).catch(res => {
              this.loading = false

            })
          }
        }
      })

    }
  }
}
