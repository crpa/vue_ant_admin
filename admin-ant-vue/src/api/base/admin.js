import request from '../../common/request'

const base="adminApi/admin";



export  function selectById(param) {
    return request({
        url: base+'/selectById',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function saveUser(param) {
    return request({
        url: base+'/saveUser',
        method: 'post',
        data: {...param, crypto:true}
    })
}
export  function deleteById(param) {
    return request({
        url: base+'/deleteById',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function deleteByIds(param) {
  return request({
    url: base+'/deleteByIds',
    method: 'post',
    data: {...param, crypto:true}
  })
}

export  function selectPage(param) {
    return request({
        url: base+'/selectPageOver',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function updatePassword(param) {
    return request({
        url: base+'/updatePassword',
        method: 'post',
        data: {...param, crypto:true}
    })
}

export  function selectUserInfo(param) {
    return request({
        url: base+'/selectUserInfo',
        method: 'post',
        data: {...param, crypto:true}
    })
}
