import request from '../../common/request'

const base="adminApi/syslog";



export  function selectById(param) {
    return request({
        url: base+'/selectById',
        method: 'post',
        data: {...param, crypto:true}
    })
}


export  function selectPage(param) {
    return request({
        url: base+'/selectPage',
        method: 'post',
        data: {...param, crypto:true}
    })
}

