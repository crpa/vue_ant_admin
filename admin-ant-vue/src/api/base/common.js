
import request, {requestMultipart} from '../../common/request'
const base="common";


export  function upload(param) {
    return requestMultipart({
        url: base+'/upload',
        method: 'post',
        data: param
    })
}
export  function selectUserInfo(param) {
    return request({
        url:  base+'/selectUserInfo',
        method: 'post',
        data: {...param, crypto:true}
    })
}
export  function login(param) {
    return request({
        url:  base+'/login',
        method: 'post',
        data: {...param, crypto:true}
    })
}
