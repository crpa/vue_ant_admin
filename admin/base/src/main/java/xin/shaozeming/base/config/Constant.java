package xin.shaozeming.base.config;

/**
 * @author: 邵泽铭
 * @date: 2019/8/22
 * @description:
 **/
public class Constant {
    public static final String redis_register_code="register_code_";
    public static final String redis_reset_code="reset_code_";
    public static final String redis_token="token_";

    public static final String path_org_logo="orglogo/";

    public static final Integer redis_token_expire=60*60*24*30;

    public static final Integer order_expire=1; /*小时*/

    public static final String im_avatar="https://hzhonghudev.oss-cn-beijing.aliyuncs.com/%E5%9C%A8%E7%BA%BF%E5%AE%A2%E6%9C%8D.png";
}
