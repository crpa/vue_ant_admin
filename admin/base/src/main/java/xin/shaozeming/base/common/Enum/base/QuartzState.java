package xin.shaozeming.base.common.Enum.base;

/**
 * @author: 邵泽铭
 * @date: 2019/8/26
 * @description:
 **/
public enum QuartzState {
    START( 0,"启动"),
    STOP( 1,"暂停"),
    ;
    private Integer code;
    private String value;
    private QuartzState(Integer code, String value){
        this.code=code;
        this.value=value;
    }
    public Integer getCode(){
        return this.code;
    }
    public String getValue(){
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
