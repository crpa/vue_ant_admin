package xin.shaozeming.base.common.Enum.base;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author: 邵泽铭
 * @date: 2019/8/26
 * @description:
 **/
public enum LoggerType implements IEnum<Integer> {


    SELECT( 0,"查询"),
    ADD( 1,"添加"),
    DELETE( 2,"删除"),
    UPDATE( 3,"修改"),
    IMPORT( 4,"导入"),
    EXPORT( 5,"导出"),
    SAVE( 6,"添加或修改"),
    ;
    private Integer code;
    private String desc;

    private LoggerType(Integer code, String desc){
        this.code=code;
        this.desc=desc;
    }
    public Integer getCode(){
        return this.code;
    }
    @JsonValue
    public String getDesc(){
        return this.desc;
    }

    @Override
    public String toString() {
        return this.desc;
    }


    @Override
    public Integer getValue() {
        return this.code;
    }
}
