package xin.shaozeming.base.common.servlet.interceptor;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import xin.shaozeming.base.common.Enum.base.State;
import xin.shaozeming.base.common.Response;
import xin.shaozeming.base.common.aop.annotation.Permission;
import xin.shaozeming.base.common.aop.annotation.TokenCheck;
import xin.shaozeming.base.common.utils.ApplicationContextHolder;
import xin.shaozeming.base.common.utils.DateUtils;
import xin.shaozeming.base.common.utils.TokenUtils;
import xin.shaozeming.base.po.UserPO;
import xin.shaozeming.base.service.PermissionService;
import xin.shaozeming.base.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author: 邵泽铭
 * @date: 2019/10/16
 * @description:
 **/

@Log4j2
public class PermissionInterceptor extends HandlerInterceptorAdapter {


    private PermissionService permissionService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(permissionService==null){
            permissionService=ApplicationContextHolder.getApplicationContext().getBean(PermissionService.class);
        }

        if (handler instanceof HandlerMethod ) {


            /*获取类上的权限注解*/
            Permission classPermission = ((HandlerMethod) handler).getBeanType().getAnnotation(Permission.class);

            /*获取方法上的权限注解*/
            Permission methodPermission = ((HandlerMethod) handler).getMethod().getAnnotation(Permission.class);


            List<String> needPermission=new ArrayList<>();
            String group="";


            /*获取类上的权限,类上的权限每个方法都具有*/
            if(classPermission!=null){

                /*获取类上的权限组,权限组适用于BaseController中的通用方法*/
                group= classPermission.group();
                if(!StringUtils.isEmpty(classPermission.code())){
                    needPermission.addAll(Arrays.asList(classPermission.code().split(","))) ;
                }
            }

            /*获取方法上的权限,*/
            if(methodPermission!=null){
                if(!StringUtils.isEmpty(methodPermission.code())){
                    String[] permissions=methodPermission.code().split(",");
                    needPermission.addAll(Arrays.asList(permissions));
                }
                /*BaseController中的通用方法上的完整权限是 类上的权限组+ : + 通用方法上的部分*/
                /*类上没有权限组，该权限不生效*/
                if((!StringUtils.isEmpty(methodPermission.part()))&&(!StringUtils.isEmpty(group))){
                    String[] permissions=methodPermission.part().split(",");
                    for (int i=0;i<permissions.length;i++){
                                permissions[i]=(group+":"+permissions[i]);
                                needPermission.add(permissions[i]);
                    }
                }
            }

            if(needPermission.size()>0){
                String token=TokenUtils.getToken(request);
                if(StringUtils.isEmpty(token)){
                    InterceptorConfig.returnResponse(response,new Response<>(State.RES_NOLOGIN.getCode()));
                    return false;
                }
                if(permissionService.checkUserPermission(token,needPermission )==0){
                    InterceptorConfig.returnResponse(response,new Response<>(State.RES_NOPERMIT.getCode()));
                    return false;
                }
            }

        }

        return super.preHandle(request, response, handler);
    }



}
