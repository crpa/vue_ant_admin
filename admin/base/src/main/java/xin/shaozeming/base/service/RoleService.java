package xin.shaozeming.base.service;

import org.springframework.transaction.annotation.Transactional;
import xin.shaozeming.base.po.RolePO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-08-22
 */

public interface RoleService extends IService<RolePO> {


    /**
     * 查询其他角色
     * @param list
     * @return
     */
    List<RolePO> selectRoleNotIn(List<Integer> list);
}
