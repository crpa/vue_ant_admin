/*
package xin.shaozeming.base.common.aop;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import xin.shaozeming.base.common.utils.AESUtils;
import xin.shaozeming.base.common.utils.MapToObjectUtil;
import xin.shaozeming.base.common.utils.ObjectToMapUtil;
import xin.shaozeming.base.common.utils.RSAUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

*/
/**
 * Created by zhao on 2017-02-28 23:19.
 *//*


@Component
@Aspect
@Order(1)
public class ParamsDecryptedAspect {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Pointcut("@annotation(xin.shaozeming.base.common.aop.annotation.ParamsDecrypted)")
    private void cutPoint(){};

    @Around("cutPoint()")
    private Object process(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        */
/** 获取入参value数组 *//*

        Object[] args = proceedingJoinPoint.getArgs();
        */
/** 密钥 *//*

        String key = null;
        for (int i = 0; i < args.length; i++) {
            if(!StringUtils.isEmpty(args[i]) &&  args[i].getClass()==String.class){
                if(i == 0) {
                    */
/** 获取密钥 *//*

                    String keys = args[i].toString();
                    key = RSAUtils.decryptByPrivateKey(keys);
                    args[i] = args[i];
                }else {
                    args[i] = AESUtils.decryptData(key,args[i].toString());
                }
            }else {
                Object object = args[i];
                if(!StringUtils.isEmpty(object)) {
                    if(object instanceof HttpServletRequest || object instanceof HttpServletResponse){
                        continue;
                    }

                    */
/** 加密数据 *//*

                    Map<String,Object> encryptMap = ObjectToMapUtil.transBean2Map(object);
                    */
/** 解密数据 *//*

                    Map<String,Object> decryptMap = new HashMap<String,Object>();
                    for (Map.Entry<String, Object> m :encryptMap.entrySet())  {
                        if(!StringUtils.isEmpty(m.getValue())) {
                            decryptMap.put(m.getKey(),AESUtils.decryptData(key,m.getValue().toString()));
                        }else {
                            decryptMap.put(m.getKey(),m.getValue());
                        }
                    }
                    MapToObjectUtil.transfer(decryptMap,args[i]);
                }
            }
        }
        Object proceed = proceedingJoinPoint.proceed(args);
        return proceed;
    }
}
*/
