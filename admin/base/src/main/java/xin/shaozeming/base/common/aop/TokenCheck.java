/*
package xin.shaozeming.base.common.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import xin.shaozeming.base.common.Enum.base.State;
import xin.shaozeming.base.common.Response;
import xin.shaozeming.base.common.aop.annotation.Permission;
import xin.shaozeming.base.common.utils.DateUtils;
import xin.shaozeming.base.common.utils.TokenUtils;
import xin.shaozeming.base.po.UserPO;
import xin.shaozeming.base.service.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

*/
/**
 * @author: 邵泽铭
 * @date: 2019/4/17
 * @description:
 **//*


@Component
@Aspect
@Order(4)
public class TokenCheck {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private UserService userService;

    @Pointcut("@annotation(xin.shaozeming.base.common.aop.annotation.TokenCheck)")
    private void cutPoint(){};

    @Around("cutPoint()")
    private Object process(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        */
/** 获取入参value数组 *//*

        Object[] args = proceedingJoinPoint.getArgs();
        if (args.length<1){
            return new Response<>(State.RES_PARAMERROR.getCode());
        }
        String token=(args[0]!=null && args[0] instanceof HttpServletRequest)?TokenUtils.getToken((HttpServletRequest) args[0]) :"";
        if(StringUtils.isEmpty(token)){
            return new Response<>(State.RES_PARAMERROR.getCode());
        }
        */
/*查看会话token是否失效*//*

        UserPO userPO= userService.checkToken(token);
        if(userPO==null){
            return new Response<>(State.RES_NOLOGIN.getCode());
        }
        */
/*延长token失效时间*//*

        userPO.setTokenExpire(DateUtils.addDate(new Date(), 3));
        if(!userService.updateById(userPO)){
            return new Response<>(State.RES_NOLOGIN.getCode());
        }

        return proceedingJoinPoint.proceed(args);

    }
}
*/
