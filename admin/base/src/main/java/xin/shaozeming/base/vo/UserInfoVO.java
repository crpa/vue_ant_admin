package xin.shaozeming.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author: 邵泽铭
 * @date: 2020/1/10
 * @description:
 **/
@Data
public class UserInfoVO {


    @ApiModelProperty(value = "用户")
    private UserVO user;

    @ApiModelProperty(value = "权限")
    private List<PermissionVO> permissions;
}
