package xin.shaozeming.base.dao;

import org.apache.ibatis.annotations.Param;
import xin.shaozeming.base.dto.RoleUserDTO;
import xin.shaozeming.base.po.UserRolePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-08-22
 */
public interface UserRoleDao extends BaseMapper<UserRolePO> {

    /**
     * 查询用户列表的角色
     * @param ids
     * @return
     */
    List<RoleUserDTO> selectRoleUserIn(@Param("ids") List<Integer> ids);
}
