package xin.shaozeming.base.common.servlet.listen;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


@WebListener
public class myServletContextListener  implements ServletContextListener {

    @Autowired
    private ApplicationContext appContext;
    @Override
    public void contextInitialized(ServletContextEvent sce) {

        try {

        } catch (Exception e) {
            SpringApplication.exit(appContext, () -> 0);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("contextDestroyed");
    }

}
