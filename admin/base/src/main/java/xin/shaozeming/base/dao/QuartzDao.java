package xin.shaozeming.base.dao;

import xin.shaozeming.base.po.QuartzPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-17
 */
public interface QuartzDao extends BaseMapper<QuartzPO> {

}
