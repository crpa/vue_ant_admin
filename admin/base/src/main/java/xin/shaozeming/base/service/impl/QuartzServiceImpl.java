package xin.shaozeming.base.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import xin.shaozeming.base.common.Enum.base.QuartzState;
import xin.shaozeming.base.common.ex.CustomerException;
import xin.shaozeming.base.common.quartz.DynamicQuartzScheduler;
import xin.shaozeming.base.po.QuartzPO;
import xin.shaozeming.base.dao.QuartzDao;
import xin.shaozeming.base.service.QuartzService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-17
 */
@Log4j2
@Transactional
@Service
public class QuartzServiceImpl extends ServiceImpl<QuartzDao, QuartzPO> implements QuartzService {

    @Resource
    private DynamicQuartzScheduler dynamicQuartzScheduler;

    /**
     * 保存或更新定时器
     *
     * @param quartzPO
     * @return
     */
    @Override
    public boolean saveQuartz(QuartzPO quartzPO) {

        if (StringUtils.isEmpty(quartzPO.getId())) {
            quartzPO.setCreatedTime(new Date());
            quartzPO.setModifiedTime(new Date());
            if (!save(quartzPO)) {
                throw new CustomerException("保存定时器失败");
            }
            try {
                dynamicQuartzScheduler.startJob(quartzPO.getId(), quartzPO.getCron(), quartzPO.getQuartzName());
            } catch (Exception e) {
                log.error("定时器启动错误:{}",e.getMessage() );
                throw new CustomerException("定时器无法启动");
            }
        } else {
            quartzPO.setModifiedTime(new Date());
            if (!updateById(quartzPO)) {
                throw new CustomerException("保存定时器失败");
            }
            try {
                dynamicQuartzScheduler.modifyJob(quartzPO.getId(), quartzPO.getCron());
            } catch (Exception e) {
                log.error("定时器启动错误:{}",e.getMessage() );
                throw new CustomerException("定时器无法启动");
            }

        }
        return true;
    }

    /**
     * 更新定时器状态
     *
     * @param quartzPO
     * @return
     */
    @Override
    public boolean updateQuartzState(QuartzPO quartzPO) {

        if(!updateById(quartzPO)){
            throw new CustomerException("定时器状态变更失败");
        }
        if(QuartzState.START.getCode().equals(quartzPO.getQuartzState())){
            try {
                dynamicQuartzScheduler.resumeJob(quartzPO.getId());
            } catch (Exception e) {
                log.error("定时器启动错误:{}",e.getMessage() );
                throw new CustomerException("定时器无法启动");
            }
        }else {
            try {
                dynamicQuartzScheduler.pauseJob(quartzPO.getId());
            } catch (Exception e) {
                log.error("定时器暂停错误:{}",e.getMessage() );
                throw new CustomerException("定时器无法暂停");
            }
        }
        return true;
    }
}
