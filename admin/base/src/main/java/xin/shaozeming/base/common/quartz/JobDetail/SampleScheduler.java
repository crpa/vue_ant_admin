//package xin.shaozeming.base.common.quartz.JobDetail;
//
//import org.quartz.*;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import xin.shaozeming.base.common.quartz.job.SampleJob;
//
///**
// * @author: 邵泽铭
// * @date: 2019/9/24
// * @description:
// **/
//@Configuration
//public class SampleScheduler {
//    @Bean
//    public JobDetail sampleJobDetail() {
//        // 链式编程,可以携带多个参数,在Job类中声明属性 + setter方法
//        return JobBuilder.newJob(SampleJob.class)
//                .withIdentity("sampleJob")
//                .storeDurably() //即使没有Trigger关联时，也不需要删除该JobDetail
//                .build();
//    }
//
//    @Bean
//    public Trigger sampleJobTrigger(){
//        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule("0 0 0/2 * * ?");
//
//        return TriggerBuilder.newTrigger()
//                .forJob(sampleJobDetail())
//                .withIdentity("sampleTrigger")
//                .withSchedule(cronScheduleBuilder).build();
//    }
//}
