package xin.shaozeming.base.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author: 邵泽铭
 * @date: 2019/4/8
 * @description:
 **/
@Data
public class PermissionVO {



    @ApiModelProperty(value = "主键id")
    private Integer id;

    @ApiModelProperty(value = "父id")
    private Integer parentId;

    @ApiModelProperty(value = "1 是根目录 0 不是根目录")
    private Boolean root;

    @ApiModelProperty(value = "目录名称")
    private String menuName;

    @ApiModelProperty(value = "前端路由地址")
    private String routerUrl;

    @ApiModelProperty(value = "目录图标")
    private String menuIcon;

    @ApiModelProperty(value = "前端组件名称")
    private String componentName;

    @ApiModelProperty(value = "排序号")
    private Integer priority;

    @ApiModelProperty(value = "0 目录 1 菜单 2 按钮/权限")
    private Integer type;

    @ApiModelProperty(value = "权限编号")
    private String permsCode;

    @ApiModelProperty(value = "子目录")
    private List<PermissionVO> children;

}
