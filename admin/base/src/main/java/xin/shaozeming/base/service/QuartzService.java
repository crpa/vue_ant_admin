package xin.shaozeming.base.service;

import xin.shaozeming.base.po.QuartzPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邵泽铭
 * @since 2019-10-17
 */
public interface QuartzService extends IService<QuartzPO> {

    /**
     *  保存或更新定时器
     * @return
     */
    boolean saveQuartz(QuartzPO quartzPO);

    /**
     *  更新定时器状态
     * @return
     */
    boolean updateQuartzState(QuartzPO quartzPO);
}
