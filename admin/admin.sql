/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : admin

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 10/01/2020 17:41:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_group
-- ----------------------------
DROP TABLE IF EXISTS `base_group`;
CREATE TABLE `base_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组名',
  `parent_id` int(11) NOT NULL COMMENT '父id',
  `created_time` datetime(0) NULL,
  `modified_time` datetime(0) NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_group_user
-- ----------------------------
DROP TABLE IF EXISTS `base_group_user`;
CREATE TABLE `base_group_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_id` int(11) NOT NULL COMMENT '组id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for base_permission
-- ----------------------------
DROP TABLE IF EXISTS `base_permission`;
CREATE TABLE `base_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父id',
  `menu_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '目录名称',
  `router_url` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '前端路由地址',
  `menu_icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '目录图标',
  `component_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '前端组件名称',
  `priority` int(11) NOT NULL DEFAULT 500 COMMENT '排序号',
  `type` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0 一级菜单 1 子菜单 2 按钮/权限',
  `perms_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '权限编号',
  `created_time` datetime(0) NOT NULL COMMENT '创建时间',
  `modified_time` datetime(0) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_component_name`(`component_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_permission
-- ----------------------------
INSERT INTO `base_permission` VALUES (1, 0, '权限设置', '/permission', 'fa fa-bug', 'permission', 9999, 0, '', '2019-04-01 16:59:30', '2019-10-15 14:32:46');
INSERT INTO `base_permission` VALUES (2, 1, '角色管理', '/permission/role', '', 'role', 2, 1, 'role:list', '2019-04-01 17:00:02', '2019-10-16 11:27:46');
INSERT INTO `base_permission` VALUES (3, 1, '目录管理', '/permission/menus', '', 'menus', 1, 1, 'menus:list', '2019-04-01 17:00:21', '2019-10-16 11:28:03');
INSERT INTO `base_permission` VALUES (4, 0, '系统', '/system', 'fa fa-window-restore', 'system', 100000, 0, '', '2019-07-23 14:53:52', '2019-10-17 17:25:15');
INSERT INTO `base_permission` VALUES (9, 1, '管理员管理', '/permission/admin', '', 'admin', 4, 1, 'admin:list', '2019-07-23 16:03:27', '2019-10-16 11:29:16');
INSERT INTO `base_permission` VALUES (10, 4, '操作日志', '/system/syslog', '', 'syslog', 1, 1, 'syslog:list', '2019-07-23 16:29:26', '2019-10-17 11:11:35');
INSERT INTO `base_permission` VALUES (11, 4, '定时任务', '/system/quartz', '', 'quartz', 4, 1, 'quartz:list', '2019-10-17 16:53:44', '2019-10-17 16:53:44');

-- ----------------------------
-- Table structure for base_quartz
-- ----------------------------
DROP TABLE IF EXISTS `base_quartz`;
CREATE TABLE `base_quartz`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `quartz_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名',
  `cron` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `param` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `quartz_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `quartz_state` tinyint(2) NULL DEFAULT 0 COMMENT '0 启动 1暂停',
  `created_time` datetime(0) NULL DEFAULT NULL,
  `modified_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_quartz
-- ----------------------------
INSERT INTO `base_quartz` VALUES (1, 'SampleJob', '0/2 * * * * ?', NULL, '简单测试定时器', 1, '2019-10-17 00:00:00', '2019-10-17 00:00:00');

-- ----------------------------
-- Table structure for base_role
-- ----------------------------
DROP TABLE IF EXISTS `base_role`;
CREATE TABLE `base_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `created_time` datetime(0) NULL,
  `modified_time` datetime(0) NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_name`(`role_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_role
-- ----------------------------
INSERT INTO `base_role` VALUES (1, '管理员', '2019-04-01 16:57:57', '2019-07-23 14:11:34');
INSERT INTO `base_role` VALUES (3, '开发人员', '2019-07-17 16:12:00', '2019-07-23 09:44:52');

-- ----------------------------
-- Table structure for base_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `base_role_permission`;
CREATE TABLE `base_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `permission_id` int(11) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_role_permission
-- ----------------------------
INSERT INTO `base_role_permission` VALUES (67, 1, 1);
INSERT INTO `base_role_permission` VALUES (68, 1, 2);
INSERT INTO `base_role_permission` VALUES (69, 1, 3);
INSERT INTO `base_role_permission` VALUES (70, 1, 9);
INSERT INTO `base_role_permission` VALUES (71, 1, 4);
INSERT INTO `base_role_permission` VALUES (72, 1, 10);
INSERT INTO `base_role_permission` VALUES (73, 1, 11);
INSERT INTO `base_role_permission` VALUES (74, 3, 1);
INSERT INTO `base_role_permission` VALUES (75, 3, 2);
INSERT INTO `base_role_permission` VALUES (76, 3, 3);
INSERT INTO `base_role_permission` VALUES (77, 3, 9);
INSERT INTO `base_role_permission` VALUES (78, 3, 4);
INSERT INTO `base_role_permission` VALUES (79, 3, 10);
INSERT INTO `base_role_permission` VALUES (80, 3, 11);

-- ----------------------------
-- Table structure for base_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `base_sys_log`;
CREATE TABLE `base_sys_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户',
  `ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
  `content` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作内容',
  `params` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `type` tinyint(2) NULL DEFAULT NULL COMMENT '0 查询 1 添加 2 删除 3 修改 4导入 5 导出',
  `created_time` datetime(0) NULL DEFAULT NULL,
  `modified_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 367 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_sys_log
-- ----------------------------
INSERT INTO `base_sys_log` VALUES (2, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 11:10:51', '2019-10-17 11:10:51');
INSERT INTO `base_sys_log` VALUES (3, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 11:11:12', '2019-10-17 11:11:12');
INSERT INTO `base_sys_log` VALUES (4, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 11:11:12', '2019-10-17 11:11:12');
INSERT INTO `base_sys_log` VALUES (5, 'admin', '127.0.0.1', '目录修改', '{\"permsCode\":\"syslog:list\",\"children\":\"\",\"menuName\":\"操作日志\",\"id\":\"10\",\"componentName\":\"syslog\",\"priority\":\"1\",\"type\":\"1\",\"routerUrl\":\"/system/syslog\",\"parentId\":\"4\"}', 3, '2019-10-17 11:11:35', '2019-10-17 11:11:35');
INSERT INTO `base_sys_log` VALUES (6, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 11:11:35', '2019-10-17 11:11:35');
INSERT INTO `base_sys_log` VALUES (7, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 11:21:06', '2019-10-17 11:21:06');
INSERT INTO `base_sys_log` VALUES (8, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 11:21:06', '2019-10-17 11:21:06');
INSERT INTO `base_sys_log` VALUES (9, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 11:21:10', '2019-10-17 11:21:10');
INSERT INTO `base_sys_log` VALUES (10, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 11:21:35', '2019-10-17 11:21:35');
INSERT INTO `base_sys_log` VALUES (11, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 11:22:03', '2019-10-17 11:22:03');
INSERT INTO `base_sys_log` VALUES (12, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 11:33:31', '2019-10-17 11:33:31');
INSERT INTO `base_sys_log` VALUES (13, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 11:33:35', '2019-10-17 11:33:35');
INSERT INTO `base_sys_log` VALUES (14, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 11:33:35', '2019-10-17 11:33:35');
INSERT INTO `base_sys_log` VALUES (15, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 11:33:37', '2019-10-17 11:33:37');
INSERT INTO `base_sys_log` VALUES (16, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 11:33:38', '2019-10-17 11:33:38');
INSERT INTO `base_sys_log` VALUES (17, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 11:33:40', '2019-10-17 11:33:40');
INSERT INTO `base_sys_log` VALUES (18, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"2\"}', 0, '2019-10-17 11:33:50', '2019-10-17 11:33:50');
INSERT INTO `base_sys_log` VALUES (19, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 11:33:52', '2019-10-17 11:33:52');
INSERT INTO `base_sys_log` VALUES (20, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"2\"}', 0, '2019-10-17 12:43:42', '2019-10-17 12:43:42');
INSERT INTO `base_sys_log` VALUES (21, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 12:43:53', '2019-10-17 12:43:53');
INSERT INTO `base_sys_log` VALUES (22, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":0}\",\"page\":\"1\"}', 0, '2019-10-17 12:44:34', '2019-10-17 12:44:34');
INSERT INTO `base_sys_log` VALUES (23, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":0}\",\"page\":\"2\"}', 0, '2019-10-17 12:44:38', '2019-10-17 12:44:38');
INSERT INTO `base_sys_log` VALUES (24, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":0}\",\"page\":\"1\"}', 0, '2019-10-17 12:44:39', '2019-10-17 12:44:39');
INSERT INTO `base_sys_log` VALUES (25, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:24:00', '2019-10-17 13:24:00');
INSERT INTO `base_sys_log` VALUES (26, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:25:33', '2019-10-17 13:25:33');
INSERT INTO `base_sys_log` VALUES (27, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"createdTime\\\":[\\\"2019-10-16T16:00:00.000Z\\\",\\\"2019-10-17T16:00:00.000Z\\\"]}\",\"page\":\"1\"}', 0, '2019-10-17 13:25:42', '2019-10-17 13:25:42');
INSERT INTO `base_sys_log` VALUES (28, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"40\",\"param\":\"{\\\"createdTime\\\":[\\\"2019-10-16T16:00:00.000Z\\\",\\\"2019-10-17T16:00:00.000Z\\\"]}\",\"page\":\"1\"}', 0, '2019-10-17 13:26:09', '2019-10-17 13:26:09');
INSERT INTO `base_sys_log` VALUES (29, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"createdTime\\\":[\\\"2019-10-16T16:00:00.000Z\\\",\\\"2019-10-17T16:00:00.000Z\\\"]}\",\"page\":\"1\"}', 0, '2019-10-17 13:27:23', '2019-10-17 13:27:23');
INSERT INTO `base_sys_log` VALUES (30, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"30\",\"param\":\"{\\\"createdTime\\\":[\\\"2019-10-16T16:00:00.000Z\\\",\\\"2019-10-17T16:00:00.000Z\\\"]}\",\"page\":\"1\"}', 0, '2019-10-17 13:29:50', '2019-10-17 13:29:50');
INSERT INTO `base_sys_log` VALUES (31, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:31:54', '2019-10-17 13:31:54');
INSERT INTO `base_sys_log` VALUES (32, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:32:08', '2019-10-17 13:32:08');
INSERT INTO `base_sys_log` VALUES (33, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:33:32', '2019-10-17 13:33:32');
INSERT INTO `base_sys_log` VALUES (34, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:33:41', '2019-10-17 13:33:41');
INSERT INTO `base_sys_log` VALUES (35, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:33:56', '2019-10-17 13:33:56');
INSERT INTO `base_sys_log` VALUES (36, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:34:13', '2019-10-17 13:34:13');
INSERT INTO `base_sys_log` VALUES (37, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"40\",\"page\":\"1\"}', 0, '2019-10-17 13:34:19', '2019-10-17 13:34:19');
INSERT INTO `base_sys_log` VALUES (38, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:34:56', '2019-10-17 13:34:56');
INSERT INTO `base_sys_log` VALUES (39, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"20\",\"page\":\"1\"}', 0, '2019-10-17 13:34:57', '2019-10-17 13:34:57');
INSERT INTO `base_sys_log` VALUES (40, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:35:29', '2019-10-17 13:35:29');
INSERT INTO `base_sys_log` VALUES (41, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 13:35:44', '2019-10-17 13:35:44');
INSERT INTO `base_sys_log` VALUES (42, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 13:35:44', '2019-10-17 13:35:44');
INSERT INTO `base_sys_log` VALUES (43, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:35:56', '2019-10-17 13:35:56');
INSERT INTO `base_sys_log` VALUES (44, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:36:00', '2019-10-17 13:36:00');
INSERT INTO `base_sys_log` VALUES (45, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 13:36:02', '2019-10-17 13:36:02');
INSERT INTO `base_sys_log` VALUES (46, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 13:36:02', '2019-10-17 13:36:02');
INSERT INTO `base_sys_log` VALUES (47, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:37:30', '2019-10-17 13:37:30');
INSERT INTO `base_sys_log` VALUES (48, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 13:37:34', '2019-10-17 13:37:34');
INSERT INTO `base_sys_log` VALUES (49, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 13:37:34', '2019-10-17 13:37:34');
INSERT INTO `base_sys_log` VALUES (50, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:37:37', '2019-10-17 13:37:37');
INSERT INTO `base_sys_log` VALUES (51, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:37:41', '2019-10-17 13:37:41');
INSERT INTO `base_sys_log` VALUES (52, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:37:49', '2019-10-17 13:37:49');
INSERT INTO `base_sys_log` VALUES (53, 'admin', '127.0.0.1', '角色更新权限', '{\"ids\":\"[1,2,3,9,4,10]\",\"id\":\"3\"}', 3, '2019-10-17 13:37:59', '2019-10-17 13:37:59');
INSERT INTO `base_sys_log` VALUES (54, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:38:03', '2019-10-17 13:38:03');
INSERT INTO `base_sys_log` VALUES (55, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"2\"}', 0, '2019-10-17 13:51:32', '2019-10-17 13:51:32');
INSERT INTO `base_sys_log` VALUES (56, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"3\"}', 0, '2019-10-17 13:51:33', '2019-10-17 13:51:33');
INSERT INTO `base_sys_log` VALUES (57, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"4\"}', 0, '2019-10-17 13:51:34', '2019-10-17 13:51:34');
INSERT INTO `base_sys_log` VALUES (58, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"5\"}', 0, '2019-10-17 13:51:35', '2019-10-17 13:51:35');
INSERT INTO `base_sys_log` VALUES (59, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"6\"}', 0, '2019-10-17 13:51:35', '2019-10-17 13:51:35');
INSERT INTO `base_sys_log` VALUES (60, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:51:39', '2019-10-17 13:51:39');
INSERT INTO `base_sys_log` VALUES (61, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 13:51:41', '2019-10-17 13:51:41');
INSERT INTO `base_sys_log` VALUES (62, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 14:02:26', '2019-10-17 14:02:26');
INSERT INTO `base_sys_log` VALUES (63, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 14:02:26', '2019-10-17 14:02:26');
INSERT INTO `base_sys_log` VALUES (64, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:02:28', '2019-10-17 14:02:28');
INSERT INTO `base_sys_log` VALUES (65, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:02:29', '2019-10-17 14:02:29');
INSERT INTO `base_sys_log` VALUES (66, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:02:31', '2019-10-17 14:02:31');
INSERT INTO `base_sys_log` VALUES (67, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:02:32', '2019-10-17 14:02:32');
INSERT INTO `base_sys_log` VALUES (68, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:02:34', '2019-10-17 14:02:34');
INSERT INTO `base_sys_log` VALUES (69, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:02:35', '2019-10-17 14:02:35');
INSERT INTO `base_sys_log` VALUES (70, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 14:02:37', '2019-10-17 14:02:37');
INSERT INTO `base_sys_log` VALUES (71, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 14:02:37', '2019-10-17 14:02:37');
INSERT INTO `base_sys_log` VALUES (72, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:02:40', '2019-10-17 14:02:40');
INSERT INTO `base_sys_log` VALUES (73, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:02:42', '2019-10-17 14:02:42');
INSERT INTO `base_sys_log` VALUES (74, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:02:55', '2019-10-17 14:02:55');
INSERT INTO `base_sys_log` VALUES (75, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 14:03:05', '2019-10-17 14:03:05');
INSERT INTO `base_sys_log` VALUES (76, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 14:03:05', '2019-10-17 14:03:05');
INSERT INTO `base_sys_log` VALUES (77, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 14:28:38', '2019-10-17 14:28:38');
INSERT INTO `base_sys_log` VALUES (78, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:28:51', '2019-10-17 14:28:51');
INSERT INTO `base_sys_log` VALUES (79, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:29:03', '2019-10-17 14:29:03');
INSERT INTO `base_sys_log` VALUES (80, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"createdTime\\\":[\\\"2019-10-09T16:00:00.000Z\\\",\\\"2019-11-09T16:00:00.000Z\\\"]}\",\"page\":\"1\"}', 0, '2019-10-17 14:29:07', '2019-10-17 14:29:07');
INSERT INTO `base_sys_log` VALUES (81, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"createdTime\\\":[\\\"2019-10-09T16:00:00.000Z\\\",\\\"2019-11-09T16:00:00.000Z\\\"]}\",\"page\":\"1\"}', 0, '2019-10-17 14:31:38', '2019-10-17 14:31:38');
INSERT INTO `base_sys_log` VALUES (82, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 14:32:25', '2019-10-17 14:32:25');
INSERT INTO `base_sys_log` VALUES (83, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 14:32:25', '2019-10-17 14:32:25');
INSERT INTO `base_sys_log` VALUES (84, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 14:32:29', '2019-10-17 14:32:29');
INSERT INTO `base_sys_log` VALUES (85, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 16:52:53', '2019-10-17 16:52:53');
INSERT INTO `base_sys_log` VALUES (86, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 16:52:53', '2019-10-17 16:52:53');
INSERT INTO `base_sys_log` VALUES (87, 'admin', '127.0.0.1', '目录修改', '{\"permsCode\":\"quartz:list\",\"menuName\":\"定时任务\",\"componentName\":\"quartz\",\"priority\":\"4\",\"type\":\"1\",\"routerUrl\":\"/system/quartz\",\"parentId\":\"4\"}', 3, '2019-10-17 16:53:44', '2019-10-17 16:53:44');
INSERT INTO `base_sys_log` VALUES (88, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 16:53:44', '2019-10-17 16:53:44');
INSERT INTO `base_sys_log` VALUES (89, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 16:53:54', '2019-10-17 16:53:54');
INSERT INTO `base_sys_log` VALUES (90, 'admin', '127.0.0.1', '角色更新权限', '{\"ids\":\"[1,2,3,9,4,10,11]\",\"id\":\"1\"}', 3, '2019-10-17 16:53:58', '2019-10-17 16:53:58');
INSERT INTO `base_sys_log` VALUES (91, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 16:54:01', '2019-10-17 16:54:01');
INSERT INTO `base_sys_log` VALUES (92, 'admin', '127.0.0.1', '定时器修改', '{\"cron\":\"0/1 * * * * ?\",\"quartzName\":\"SampleJob\",\"quartzDesc\":\"简单测试定时器\"}', 3, '2019-10-17 16:56:32', '2019-10-17 16:56:32');
INSERT INTO `base_sys_log` VALUES (93, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 16:58:15', '2019-10-17 16:58:15');
INSERT INTO `base_sys_log` VALUES (94, 'admin', '127.0.0.1', '定时器修改', '{\"cron\":\"0/2 * * * * ?\",\"modifiedTime\":\"2019-10-17 16:56:32\",\"quartzState\":\"0\",\"createdTime\":\"2019-10-17 16:56:32\",\"id\":\"1\",\"quartzName\":\"SampleJob\",\"quartzDesc\":\"简单测试定时器\"}', 3, '2019-10-17 17:02:59', '2019-10-17 17:02:59');
INSERT INTO `base_sys_log` VALUES (95, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:02:59', '2019-10-17 17:02:59');
INSERT INTO `base_sys_log` VALUES (96, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:12:24', '2019-10-17 17:12:24');
INSERT INTO `base_sys_log` VALUES (97, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:15:15', '2019-10-17 17:15:15');
INSERT INTO `base_sys_log` VALUES (98, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:17:34', '2019-10-17 17:17:34');
INSERT INTO `base_sys_log` VALUES (99, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:17:53', '2019-10-17 17:17:53');
INSERT INTO `base_sys_log` VALUES (100, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:18:40', '2019-10-17 17:18:40');
INSERT INTO `base_sys_log` VALUES (101, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:19:43', '2019-10-17 17:19:43');
INSERT INTO `base_sys_log` VALUES (102, 'admin', '127.0.0.1', '定时器状态更新', '{\"cron\":\"0/2 * * * * ?\",\"modifiedTime\":\"2019-10-17 17:02:59\",\"quartzState\":\"1\",\"createdTime\":\"2019-10-17 00:00:00\",\"id\":\"1\",\"quartzName\":\"SampleJob\",\"quartzDesc\":\"简单测试定时器\"}', 3, '2019-10-17 17:21:43', '2019-10-17 17:21:43');
INSERT INTO `base_sys_log` VALUES (103, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:21:44', '2019-10-17 17:21:44');
INSERT INTO `base_sys_log` VALUES (104, 'admin', '127.0.0.1', '定时器状态更新', '{\"cron\":\"0/2 * * * * ?\",\"modifiedTime\":\"2019-10-17 00:00:00\",\"quartzState\":\"0\",\"createdTime\":\"2019-10-17 00:00:00\",\"id\":\"1\",\"quartzName\":\"SampleJob\",\"quartzDesc\":\"简单测试定时器\"}', 3, '2019-10-17 17:22:09', '2019-10-17 17:22:09');
INSERT INTO `base_sys_log` VALUES (105, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:22:09', '2019-10-17 17:22:09');
INSERT INTO `base_sys_log` VALUES (106, 'admin', '127.0.0.1', '定时器状态更新', '{\"cron\":\"0/2 * * * * ?\",\"modifiedTime\":\"2019-10-17 00:00:00\",\"quartzState\":\"1\",\"createdTime\":\"2019-10-17 00:00:00\",\"id\":\"1\",\"quartzName\":\"SampleJob\",\"quartzDesc\":\"简单测试定时器\"}', 3, '2019-10-17 17:22:24', '2019-10-17 17:22:24');
INSERT INTO `base_sys_log` VALUES (107, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:22:24', '2019-10-17 17:22:24');
INSERT INTO `base_sys_log` VALUES (108, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:22:35', '2019-10-17 17:22:35');
INSERT INTO `base_sys_log` VALUES (109, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:22:35', '2019-10-17 17:22:35');
INSERT INTO `base_sys_log` VALUES (110, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:22:39', '2019-10-17 17:22:39');
INSERT INTO `base_sys_log` VALUES (111, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:22:40', '2019-10-17 17:22:40');
INSERT INTO `base_sys_log` VALUES (112, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:22:46', '2019-10-17 17:22:46');
INSERT INTO `base_sys_log` VALUES (113, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:23:02', '2019-10-17 17:23:02');
INSERT INTO `base_sys_log` VALUES (114, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:25:04', '2019-10-17 17:25:04');
INSERT INTO `base_sys_log` VALUES (115, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:25:04', '2019-10-17 17:25:04');
INSERT INTO `base_sys_log` VALUES (116, 'admin', '127.0.0.1', '目录修改', '{\"menuIcon\":\"fa fa-window-restore\",\"children\":\"[object Object],[object Object]\",\"menuName\":\"系统\",\"id\":\"4\",\"componentName\":\"system\",\"priority\":\"100000\",\"type\":\"0\",\"routerUrl\":\"/system\",\"parentId\":\"0\"}', 3, '2019-10-17 17:25:15', '2019-10-17 17:25:15');
INSERT INTO `base_sys_log` VALUES (117, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:25:15', '2019-10-17 17:25:15');
INSERT INTO `base_sys_log` VALUES (118, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:25:18', '2019-10-17 17:25:18');
INSERT INTO `base_sys_log` VALUES (119, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:25:18', '2019-10-17 17:25:18');
INSERT INTO `base_sys_log` VALUES (120, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:25:22', '2019-10-17 17:25:22');
INSERT INTO `base_sys_log` VALUES (121, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:25:24', '2019-10-17 17:25:24');
INSERT INTO `base_sys_log` VALUES (122, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:25:25', '2019-10-17 17:25:25');
INSERT INTO `base_sys_log` VALUES (123, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:25:28', '2019-10-17 17:25:28');
INSERT INTO `base_sys_log` VALUES (124, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:25:28', '2019-10-17 17:25:28');
INSERT INTO `base_sys_log` VALUES (125, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:25:29', '2019-10-17 17:25:29');
INSERT INTO `base_sys_log` VALUES (126, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:25:31', '2019-10-17 17:25:31');
INSERT INTO `base_sys_log` VALUES (127, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:25:34', '2019-10-17 17:25:34');
INSERT INTO `base_sys_log` VALUES (128, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:25:34', '2019-10-17 17:25:34');
INSERT INTO `base_sys_log` VALUES (129, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:25:39', '2019-10-17 17:25:39');
INSERT INTO `base_sys_log` VALUES (130, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:25:41', '2019-10-17 17:25:41');
INSERT INTO `base_sys_log` VALUES (131, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:28:10', '2019-10-17 17:28:10');
INSERT INTO `base_sys_log` VALUES (132, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:28:10', '2019-10-17 17:28:10');
INSERT INTO `base_sys_log` VALUES (133, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:28:11', '2019-10-17 17:28:11');
INSERT INTO `base_sys_log` VALUES (134, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:28:14', '2019-10-17 17:28:14');
INSERT INTO `base_sys_log` VALUES (135, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:28:17', '2019-10-17 17:28:17');
INSERT INTO `base_sys_log` VALUES (136, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:28:29', '2019-10-17 17:28:29');
INSERT INTO `base_sys_log` VALUES (137, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:28:29', '2019-10-17 17:28:29');
INSERT INTO `base_sys_log` VALUES (138, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:29:10', '2019-10-17 17:29:10');
INSERT INTO `base_sys_log` VALUES (139, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:29:12', '2019-10-17 17:29:12');
INSERT INTO `base_sys_log` VALUES (140, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:29:21', '2019-10-17 17:29:21');
INSERT INTO `base_sys_log` VALUES (141, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:29:23', '2019-10-17 17:29:23');
INSERT INTO `base_sys_log` VALUES (142, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:29:27', '2019-10-17 17:29:27');
INSERT INTO `base_sys_log` VALUES (143, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2019-10-17 17:32:07', '2019-10-17 17:32:07');
INSERT INTO `base_sys_log` VALUES (144, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:32:12', '2019-10-17 17:32:12');
INSERT INTO `base_sys_log` VALUES (145, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:32:12', '2019-10-17 17:32:12');
INSERT INTO `base_sys_log` VALUES (146, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:32:15', '2019-10-17 17:32:15');
INSERT INTO `base_sys_log` VALUES (147, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:32:28', '2019-10-17 17:32:28');
INSERT INTO `base_sys_log` VALUES (148, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:32:33', '2019-10-17 17:32:33');
INSERT INTO `base_sys_log` VALUES (149, 'admin', '127.0.0.1', '角色更新权限', '{\"ids\":\"[1,2,3,9,4,10,11]\",\"id\":\"3\"}', 3, '2019-10-17 17:32:38', '2019-10-17 17:32:38');
INSERT INTO `base_sys_log` VALUES (150, 'szm', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"szm\"}', 0, '2019-10-17 17:32:48', '2019-10-17 17:32:48');
INSERT INTO `base_sys_log` VALUES (151, 'szm', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:32:51', '2019-10-17 17:32:51');
INSERT INTO `base_sys_log` VALUES (152, 'szm', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:32:51', '2019-10-17 17:32:51');
INSERT INTO `base_sys_log` VALUES (153, 'szm', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:32:54', '2019-10-17 17:32:54');
INSERT INTO `base_sys_log` VALUES (154, 'szm', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"2\"}', 0, '2019-10-17 17:33:00', '2019-10-17 17:33:00');
INSERT INTO `base_sys_log` VALUES (155, 'szm', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:33:01', '2019-10-17 17:33:01');
INSERT INTO `base_sys_log` VALUES (156, 'szm', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:33:38', '2019-10-17 17:33:38');
INSERT INTO `base_sys_log` VALUES (157, 'szm', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:33:38', '2019-10-17 17:33:38');
INSERT INTO `base_sys_log` VALUES (158, 'szm', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:33:40', '2019-10-17 17:33:40');
INSERT INTO `base_sys_log` VALUES (159, 'szm', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:33:45', '2019-10-17 17:33:45');
INSERT INTO `base_sys_log` VALUES (160, 'szm', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-17 17:33:45', '2019-10-17 17:33:45');
INSERT INTO `base_sys_log` VALUES (161, 'szm', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:34:37', '2019-10-17 17:34:37');
INSERT INTO `base_sys_log` VALUES (162, 'szm', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-17 17:34:41', '2019-10-17 17:34:41');
INSERT INTO `base_sys_log` VALUES (163, 'szm', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:34:46', '2019-10-17 17:34:46');
INSERT INTO `base_sys_log` VALUES (164, 'szm', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"17\"}', 0, '2019-10-17 17:34:48', '2019-10-17 17:34:48');
INSERT INTO `base_sys_log` VALUES (165, 'szm', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-17 17:34:50', '2019-10-17 17:34:50');
INSERT INTO `base_sys_log` VALUES (166, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2019-10-18 14:38:42', '2019-10-18 14:38:42');
INSERT INTO `base_sys_log` VALUES (167, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:38:44', '2019-10-18 14:38:44');
INSERT INTO `base_sys_log` VALUES (168, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:38:44', '2019-10-18 14:38:44');
INSERT INTO `base_sys_log` VALUES (169, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:38:46', '2019-10-18 14:38:46');
INSERT INTO `base_sys_log` VALUES (170, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:38:48', '2019-10-18 14:38:48');
INSERT INTO `base_sys_log` VALUES (171, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:39:02', '2019-10-18 14:39:02');
INSERT INTO `base_sys_log` VALUES (172, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-18 14:39:07', '2019-10-18 14:39:07');
INSERT INTO `base_sys_log` VALUES (173, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:12', '2019-10-18 14:39:12');
INSERT INTO `base_sys_log` VALUES (174, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:12', '2019-10-18 14:39:12');
INSERT INTO `base_sys_log` VALUES (175, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:39:14', '2019-10-18 14:39:14');
INSERT INTO `base_sys_log` VALUES (176, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:17', '2019-10-18 14:39:17');
INSERT INTO `base_sys_log` VALUES (177, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:17', '2019-10-18 14:39:17');
INSERT INTO `base_sys_log` VALUES (178, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:39:19', '2019-10-18 14:39:19');
INSERT INTO `base_sys_log` VALUES (179, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:21', '2019-10-18 14:39:21');
INSERT INTO `base_sys_log` VALUES (180, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:21', '2019-10-18 14:39:21');
INSERT INTO `base_sys_log` VALUES (181, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:39:23', '2019-10-18 14:39:23');
INSERT INTO `base_sys_log` VALUES (182, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:27', '2019-10-18 14:39:27');
INSERT INTO `base_sys_log` VALUES (183, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:27', '2019-10-18 14:39:27');
INSERT INTO `base_sys_log` VALUES (184, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:39:29', '2019-10-18 14:39:29');
INSERT INTO `base_sys_log` VALUES (185, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:39:31', '2019-10-18 14:39:31');
INSERT INTO `base_sys_log` VALUES (186, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:39:58', '2019-10-18 14:39:58');
INSERT INTO `base_sys_log` VALUES (187, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:59', '2019-10-18 14:39:59');
INSERT INTO `base_sys_log` VALUES (188, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:39:59', '2019-10-18 14:39:59');
INSERT INTO `base_sys_log` VALUES (189, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:40:00', '2019-10-18 14:40:00');
INSERT INTO `base_sys_log` VALUES (190, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:40:03', '2019-10-18 14:40:03');
INSERT INTO `base_sys_log` VALUES (191, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:41:07', '2019-10-18 14:41:07');
INSERT INTO `base_sys_log` VALUES (192, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:45:17', '2019-10-18 14:45:17');
INSERT INTO `base_sys_log` VALUES (193, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:46:25', '2019-10-18 14:46:25');
INSERT INTO `base_sys_log` VALUES (194, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-18 14:46:55', '2019-10-18 14:46:55');
INSERT INTO `base_sys_log` VALUES (195, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:47:09', '2019-10-18 14:47:09');
INSERT INTO `base_sys_log` VALUES (196, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:47:09', '2019-10-18 14:47:09');
INSERT INTO `base_sys_log` VALUES (197, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:47:11', '2019-10-18 14:47:11');
INSERT INTO `base_sys_log` VALUES (198, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-18 14:47:17', '2019-10-18 14:47:17');
INSERT INTO `base_sys_log` VALUES (199, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:49:35', '2019-10-18 14:49:35');
INSERT INTO `base_sys_log` VALUES (200, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:52:23', '2019-10-18 14:52:23');
INSERT INTO `base_sys_log` VALUES (201, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:52:27', '2019-10-18 14:52:27');
INSERT INTO `base_sys_log` VALUES (202, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:57:42', '2019-10-18 14:57:42');
INSERT INTO `base_sys_log` VALUES (203, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-10-18 14:57:42', '2019-10-18 14:57:42');
INSERT INTO `base_sys_log` VALUES (204, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:57:44', '2019-10-18 14:57:44');
INSERT INTO `base_sys_log` VALUES (205, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:57:45', '2019-10-18 14:57:45');
INSERT INTO `base_sys_log` VALUES (206, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:57:47', '2019-10-18 14:57:47');
INSERT INTO `base_sys_log` VALUES (207, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-10-18 14:57:49', '2019-10-18 14:57:49');
INSERT INTO `base_sys_log` VALUES (208, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-10-18 14:57:51', '2019-10-18 14:57:51');
INSERT INTO `base_sys_log` VALUES (209, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2019-11-13 14:15:19', '2019-11-13 14:15:19');
INSERT INTO `base_sys_log` VALUES (210, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-11-13 14:16:25', '2019-11-13 14:16:25');
INSERT INTO `base_sys_log` VALUES (211, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-11-13 14:16:25', '2019-11-13 14:16:25');
INSERT INTO `base_sys_log` VALUES (212, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"page\":\"1\"}', 0, '2019-11-13 14:17:25', '2019-11-13 14:17:25');
INSERT INTO `base_sys_log` VALUES (213, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:42:42', '2019-11-15 09:42:42');
INSERT INTO `base_sys_log` VALUES (214, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:44:55', '2019-11-15 09:44:55');
INSERT INTO `base_sys_log` VALUES (215, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:46:51', '2019-11-15 09:46:51');
INSERT INTO `base_sys_log` VALUES (216, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:46:55', '2019-11-15 09:46:55');
INSERT INTO `base_sys_log` VALUES (217, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:49:01', '2019-11-15 09:49:01');
INSERT INTO `base_sys_log` VALUES (218, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:56:19', '2019-11-15 09:56:19');
INSERT INTO `base_sys_log` VALUES (219, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:56:32', '2019-11-15 09:56:32');
INSERT INTO `base_sys_log` VALUES (220, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:56:33', '2019-11-15 09:56:33');
INSERT INTO `base_sys_log` VALUES (221, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:56:50', '2019-11-15 09:56:50');
INSERT INTO `base_sys_log` VALUES (222, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:56:51', '2019-11-15 09:56:51');
INSERT INTO `base_sys_log` VALUES (223, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:57:05', '2019-11-15 09:57:05');
INSERT INTO `base_sys_log` VALUES (224, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:57:06', '2019-11-15 09:57:06');
INSERT INTO `base_sys_log` VALUES (225, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:57:45', '2019-11-15 09:57:45');
INSERT INTO `base_sys_log` VALUES (226, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:57:48', '2019-11-15 09:57:48');
INSERT INTO `base_sys_log` VALUES (227, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:57:58', '2019-11-15 09:57:58');
INSERT INTO `base_sys_log` VALUES (228, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:58:44', '2019-11-15 09:58:44');
INSERT INTO `base_sys_log` VALUES (229, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 09:58:45', '2019-11-15 09:58:45');
INSERT INTO `base_sys_log` VALUES (230, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 10:32:10', '2019-11-15 10:32:10');
INSERT INTO `base_sys_log` VALUES (231, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 10:35:16', '2019-11-15 10:35:16');
INSERT INTO `base_sys_log` VALUES (232, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 11:17:12', '2019-11-15 11:17:12');
INSERT INTO `base_sys_log` VALUES (233, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 11:17:16', '2019-11-15 11:17:16');
INSERT INTO `base_sys_log` VALUES (234, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 11:17:17', '2019-11-15 11:17:17');
INSERT INTO `base_sys_log` VALUES (235, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 11:17:19', '2019-11-15 11:17:19');
INSERT INTO `base_sys_log` VALUES (236, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 11:18:24', '2019-11-15 11:18:24');
INSERT INTO `base_sys_log` VALUES (237, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 12:49:59', '2019-11-15 12:49:59');
INSERT INTO `base_sys_log` VALUES (238, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"type\":\"account\",\"username\":\"admin\"}', 0, '2019-11-15 12:54:09', '2019-11-15 12:54:09');
INSERT INTO `base_sys_log` VALUES (239, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2019-12-18 16:07:05', '2019-12-18 16:07:05');
INSERT INTO `base_sys_log` VALUES (240, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:07:08', '2019-12-18 16:07:08');
INSERT INTO `base_sys_log` VALUES (241, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:07:08', '2019-12-18 16:07:08');
INSERT INTO `base_sys_log` VALUES (242, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:07:32', '2019-12-18 16:07:32');
INSERT INTO `base_sys_log` VALUES (243, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:07:34', '2019-12-18 16:07:34');
INSERT INTO `base_sys_log` VALUES (244, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:07:36', '2019-12-18 16:07:36');
INSERT INTO `base_sys_log` VALUES (245, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:07:37', '2019-12-18 16:07:37');
INSERT INTO `base_sys_log` VALUES (246, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:07:37', '2019-12-18 16:07:37');
INSERT INTO `base_sys_log` VALUES (247, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:07:44', '2019-12-18 16:07:44');
INSERT INTO `base_sys_log` VALUES (248, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:07:46', '2019-12-18 16:07:46');
INSERT INTO `base_sys_log` VALUES (249, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:07:52', '2019-12-18 16:07:52');
INSERT INTO `base_sys_log` VALUES (250, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-12-18 16:07:54', '2019-12-18 16:07:54');
INSERT INTO `base_sys_log` VALUES (251, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:11:37', '2019-12-18 16:11:37');
INSERT INTO `base_sys_log` VALUES (252, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:11:39', '2019-12-18 16:11:39');
INSERT INTO `base_sys_log` VALUES (253, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:11:39', '2019-12-18 16:11:39');
INSERT INTO `base_sys_log` VALUES (254, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:11:42', '2019-12-18 16:11:42');
INSERT INTO `base_sys_log` VALUES (255, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:11:43', '2019-12-18 16:11:43');
INSERT INTO `base_sys_log` VALUES (256, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:11:45', '2019-12-18 16:11:45');
INSERT INTO `base_sys_log` VALUES (257, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:11:48', '2019-12-18 16:11:48');
INSERT INTO `base_sys_log` VALUES (258, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:11:48', '2019-12-18 16:11:48');
INSERT INTO `base_sys_log` VALUES (259, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:12:28', '2019-12-18 16:12:28');
INSERT INTO `base_sys_log` VALUES (260, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:12:29', '2019-12-18 16:12:29');
INSERT INTO `base_sys_log` VALUES (261, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:12:31', '2019-12-18 16:12:31');
INSERT INTO `base_sys_log` VALUES (262, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:12:31', '2019-12-18 16:12:31');
INSERT INTO `base_sys_log` VALUES (263, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:12:33', '2019-12-18 16:12:33');
INSERT INTO `base_sys_log` VALUES (264, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-12-18 16:12:35', '2019-12-18 16:12:35');
INSERT INTO `base_sys_log` VALUES (265, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:12:37', '2019-12-18 16:12:37');
INSERT INTO `base_sys_log` VALUES (266, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:20:30', '2019-12-18 16:20:30');
INSERT INTO `base_sys_log` VALUES (267, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:29:59', '2019-12-18 16:29:59');
INSERT INTO `base_sys_log` VALUES (268, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:31:59', '2019-12-18 16:31:59');
INSERT INTO `base_sys_log` VALUES (269, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:33:03', '2019-12-18 16:33:03');
INSERT INTO `base_sys_log` VALUES (270, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:33:55', '2019-12-18 16:33:55');
INSERT INTO `base_sys_log` VALUES (271, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-12-18 16:34:11', '2019-12-18 16:34:11');
INSERT INTO `base_sys_log` VALUES (272, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:34:13', '2019-12-18 16:34:13');
INSERT INTO `base_sys_log` VALUES (273, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:34:14', '2019-12-18 16:34:14');
INSERT INTO `base_sys_log` VALUES (274, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:34:16', '2019-12-18 16:34:16');
INSERT INTO `base_sys_log` VALUES (275, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 16:34:16', '2019-12-18 16:34:16');
INSERT INTO `base_sys_log` VALUES (276, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:34:18', '2019-12-18 16:34:18');
INSERT INTO `base_sys_log` VALUES (277, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-12-18 16:34:20', '2019-12-18 16:34:20');
INSERT INTO `base_sys_log` VALUES (278, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:34:22', '2019-12-18 16:34:22');
INSERT INTO `base_sys_log` VALUES (279, 'admin', '127.0.0.1', '操作日志查询列表', '{\"sortParam\":\"created_time\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"0\"}', 0, '2019-12-18 16:34:53', '2019-12-18 16:34:53');
INSERT INTO `base_sys_log` VALUES (280, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:34:54', '2019-12-18 16:34:54');
INSERT INTO `base_sys_log` VALUES (281, 'admin', '127.0.0.1', '操作日志查询列表', '{\"sortParam\":\"created_time\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"1\"}', 0, '2019-12-18 16:34:55', '2019-12-18 16:34:55');
INSERT INTO `base_sys_log` VALUES (282, 'admin', '127.0.0.1', '操作日志查询列表', '{\"sortParam\":\"created_time\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"0\"}', 0, '2019-12-18 16:34:56', '2019-12-18 16:34:56');
INSERT INTO `base_sys_log` VALUES (283, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:34:58', '2019-12-18 16:34:58');
INSERT INTO `base_sys_log` VALUES (284, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:43:03', '2019-12-18 16:43:03');
INSERT INTO `base_sys_log` VALUES (285, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:49:16', '2019-12-18 16:49:16');
INSERT INTO `base_sys_log` VALUES (286, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:59:04', '2019-12-18 16:59:04');
INSERT INTO `base_sys_log` VALUES (287, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 16:59:40', '2019-12-18 16:59:40');
INSERT INTO `base_sys_log` VALUES (288, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:00:56', '2019-12-18 17:00:56');
INSERT INTO `base_sys_log` VALUES (289, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"type\":\"1||2\"}', 0, '2019-12-18 17:01:00', '2019-12-18 17:01:00');
INSERT INTO `base_sys_log` VALUES (290, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"type\":\"1||2||3\"}', 0, '2019-12-18 17:01:10', '2019-12-18 17:01:10');
INSERT INTO `base_sys_log` VALUES (291, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:03:45', '2019-12-18 17:03:45');
INSERT INTO `base_sys_log` VALUES (292, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"type\":\"2||1\"}', 0, '2019-12-18 17:05:12', '2019-12-18 17:05:12');
INSERT INTO `base_sys_log` VALUES (293, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:05:53', '2019-12-18 17:05:53');
INSERT INTO `base_sys_log` VALUES (294, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":\\\"1||2\\\"}\",\"page\":\"1\"}', 0, '2019-12-18 17:05:57', '2019-12-18 17:05:57');
INSERT INTO `base_sys_log` VALUES (295, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":\\\"1||2||3\\\"}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:05', '2019-12-18 17:06:05');
INSERT INTO `base_sys_log` VALUES (296, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:13', '2019-12-18 17:06:13');
INSERT INTO `base_sys_log` VALUES (297, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":1}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:18', '2019-12-18 17:06:18');
INSERT INTO `base_sys_log` VALUES (298, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":2}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:21', '2019-12-18 17:06:21');
INSERT INTO `base_sys_log` VALUES (299, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:06:30', '2019-12-18 17:06:30');
INSERT INTO `base_sys_log` VALUES (300, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:06:30', '2019-12-18 17:06:30');
INSERT INTO `base_sys_log` VALUES (301, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:32', '2019-12-18 17:06:32');
INSERT INTO `base_sys_log` VALUES (302, 'admin', '127.0.0.1', '角色新增', '{\"roleName\":\"ss\"}', 1, '2019-12-18 17:06:36', '2019-12-18 17:06:36');
INSERT INTO `base_sys_log` VALUES (303, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:36', '2019-12-18 17:06:36');
INSERT INTO `base_sys_log` VALUES (304, 'admin', '127.0.0.1', '角色单个删除', '{\"modifiedTime\":\"2019-12-18 17:06:36\",\"roleName\":\"ss\",\"createdTime\":\"2019-12-18 17:06:36\",\"id\":\"4\"}', 2, '2019-12-18 17:06:38', '2019-12-18 17:06:38');
INSERT INTO `base_sys_log` VALUES (305, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:38', '2019-12-18 17:06:38');
INSERT INTO `base_sys_log` VALUES (306, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:41', '2019-12-18 17:06:41');
INSERT INTO `base_sys_log` VALUES (307, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":\\\"1||2\\\"}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:50', '2019-12-18 17:06:50');
INSERT INTO `base_sys_log` VALUES (308, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"createdTime\\\":[\\\"2019-12-09T16:00:00.000Z\\\",\\\"2019-12-18T16:00:00.000Z\\\"],\\\"type\\\":\\\"1||2\\\"}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:55', '2019-12-18 17:06:55');
INSERT INTO `base_sys_log` VALUES (309, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"createdTime\\\":[\\\"2019-12-09T16:00:00.000Z\\\",\\\"2019-12-18T16:00:00.000Z\\\"],\\\"type\\\":\\\"1||2||3\\\"}\",\"page\":\"1\"}', 0, '2019-12-18 17:06:57', '2019-12-18 17:06:57');
INSERT INTO `base_sys_log` VALUES (310, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"createdTime\\\":[\\\"2019-11-04T16:00:00.000Z\\\",\\\"2019-12-18T16:00:00.000Z\\\"],\\\"type\\\":\\\"1||2||3\\\"}\",\"page\":\"1\"}', 0, '2019-12-18 17:07:13', '2019-12-18 17:07:13');
INSERT INTO `base_sys_log` VALUES (311, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":\\\"1||2||3\\\"}\",\"page\":\"1\"}', 0, '2019-12-18 17:07:16', '2019-12-18 17:07:16');
INSERT INTO `base_sys_log` VALUES (312, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":\\\"1||2||3\\\"}\",\"page\":\"2\"}', 0, '2019-12-18 17:07:23', '2019-12-18 17:07:23');
INSERT INTO `base_sys_log` VALUES (313, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{\\\"type\\\":\\\"1||2||3\\\"}\",\"page\":\"1\"}', 0, '2019-12-18 17:07:28', '2019-12-18 17:07:28');
INSERT INTO `base_sys_log` VALUES (314, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:07:35', '2019-12-18 17:07:35');
INSERT INTO `base_sys_log` VALUES (315, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:08:03', '2019-12-18 17:08:03');
INSERT INTO `base_sys_log` VALUES (316, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:08:08', '2019-12-18 17:08:08');
INSERT INTO `base_sys_log` VALUES (317, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:08:08', '2019-12-18 17:08:08');
INSERT INTO `base_sys_log` VALUES (318, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:08:13', '2019-12-18 17:08:13');
INSERT INTO `base_sys_log` VALUES (319, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:08:20', '2019-12-18 17:08:20');
INSERT INTO `base_sys_log` VALUES (320, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:08:24', '2019-12-18 17:08:24');
INSERT INTO `base_sys_log` VALUES (321, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:08:24', '2019-12-18 17:08:24');
INSERT INTO `base_sys_log` VALUES (322, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:08:26', '2019-12-18 17:08:26');
INSERT INTO `base_sys_log` VALUES (323, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:09:58', '2019-12-18 17:09:58');
INSERT INTO `base_sys_log` VALUES (324, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:09:58', '2019-12-18 17:09:58');
INSERT INTO `base_sys_log` VALUES (325, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:09:59', '2019-12-18 17:09:59');
INSERT INTO `base_sys_log` VALUES (326, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:10:01', '2019-12-18 17:10:01');
INSERT INTO `base_sys_log` VALUES (327, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:10:06', '2019-12-18 17:10:06');
INSERT INTO `base_sys_log` VALUES (328, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:10:06', '2019-12-18 17:10:06');
INSERT INTO `base_sys_log` VALUES (329, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:10:08', '2019-12-18 17:10:08');
INSERT INTO `base_sys_log` VALUES (330, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:10:09', '2019-12-18 17:10:09');
INSERT INTO `base_sys_log` VALUES (331, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:10:12', '2019-12-18 17:10:12');
INSERT INTO `base_sys_log` VALUES (332, 'admin', '127.0.0.1', '定时器列表查询', '{}', 0, '2019-12-18 17:10:13', '2019-12-18 17:10:13');
INSERT INTO `base_sys_log` VALUES (333, 'admin', '127.0.0.1', '操作日志查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:10:15', '2019-12-18 17:10:15');
INSERT INTO `base_sys_log` VALUES (334, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:10:17', '2019-12-18 17:10:17');
INSERT INTO `base_sys_log` VALUES (335, 'admin', '127.0.0.1', '目录列表查询', '{}', 0, '2019-12-18 17:10:17', '2019-12-18 17:10:17');
INSERT INTO `base_sys_log` VALUES (336, 'admin', '127.0.0.1', '角色查询列表', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:10:18', '2019-12-18 17:10:18');
INSERT INTO `base_sys_log` VALUES (337, 'admin', '127.0.0.1', '用户列表查询', '{\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\"}', 0, '2019-12-18 17:10:19', '2019-12-18 17:10:19');
INSERT INTO `base_sys_log` VALUES (338, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2020-01-10 10:58:53', '2020-01-10 10:58:53');
INSERT INTO `base_sys_log` VALUES (339, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2020-01-10 11:05:25', '2020-01-10 11:05:25');
INSERT INTO `base_sys_log` VALUES (340, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2020-01-10 11:16:18', '2020-01-10 11:16:18');
INSERT INTO `base_sys_log` VALUES (341, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2020-01-10 13:42:22', '2020-01-10 13:42:22');
INSERT INTO `base_sys_log` VALUES (342, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2020-01-10 14:38:54', '2020-01-10 14:38:54');
INSERT INTO `base_sys_log` VALUES (343, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2020-01-10 14:39:12', '2020-01-10 14:39:12');
INSERT INTO `base_sys_log` VALUES (344, 'admin', '127.0.0.1', '用户登录', '{\"password\":\"123456\",\"username\":\"admin\"}', 0, '2020-01-10 14:42:06', '2020-01-10 14:42:06');
INSERT INTO `base_sys_log` VALUES (345, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:26:50', '2020-01-10 16:26:50');
INSERT INTO `base_sys_log` VALUES (346, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:27:40', '2020-01-10 16:27:40');
INSERT INTO `base_sys_log` VALUES (347, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:32:09', '2020-01-10 16:32:09');
INSERT INTO `base_sys_log` VALUES (348, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:32:25', '2020-01-10 16:32:25');
INSERT INTO `base_sys_log` VALUES (349, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:35:58', '2020-01-10 16:35:58');
INSERT INTO `base_sys_log` VALUES (350, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:36:42', '2020-01-10 16:36:42');
INSERT INTO `base_sys_log` VALUES (351, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:36:50', '2020-01-10 16:36:50');
INSERT INTO `base_sys_log` VALUES (352, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:37:17', '2020-01-10 16:37:17');
INSERT INTO `base_sys_log` VALUES (353, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:44:27', '2020-01-10 16:44:27');
INSERT INTO `base_sys_log` VALUES (354, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:45:50', '2020-01-10 16:45:50');
INSERT INTO `base_sys_log` VALUES (355, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 16:50:04', '2020-01-10 16:50:04');
INSERT INTO `base_sys_log` VALUES (356, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:03:20', '2020-01-10 17:03:20');
INSERT INTO `base_sys_log` VALUES (357, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:17:46', '2020-01-10 17:17:46');
INSERT INTO `base_sys_log` VALUES (358, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:18:20', '2020-01-10 17:18:20');
INSERT INTO `base_sys_log` VALUES (359, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:23:53', '2020-01-10 17:23:53');
INSERT INTO `base_sys_log` VALUES (360, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:26:15', '2020-01-10 17:26:15');
INSERT INTO `base_sys_log` VALUES (361, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:27:04', '2020-01-10 17:27:04');
INSERT INTO `base_sys_log` VALUES (362, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:27:24', '2020-01-10 17:27:24');
INSERT INTO `base_sys_log` VALUES (363, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:28:03', '2020-01-10 17:28:03');
INSERT INTO `base_sys_log` VALUES (364, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:30:37', '2020-01-10 17:30:37');
INSERT INTO `base_sys_log` VALUES (365, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:33:23', '2020-01-10 17:33:23');
INSERT INTO `base_sys_log` VALUES (366, 'admin', '127.0.0.1', '用户列表查询', '{\"sortParam\":\"\",\"size\":\"10\",\"param\":\"{}\",\"page\":\"1\",\"direction\":\"\"}', 0, '2020-01-10 17:33:42', '2020-01-10 17:33:42');

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户code',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `token` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'token',
  `token_expire` datetime(0) NULL DEFAULT NULL COMMENT 'token过期时间',
  `nick_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户姓名',
  `account_type` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0 普通账号 1 手机号 ',
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户手机号',
  `state` tinyint(2) NOT NULL COMMENT '0 使用 1 删除 2 停用',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '上一次登陆时间',
  `created_time` datetime(0) NOT NULL COMMENT '创建时间',
  `modified_time` datetime(0) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES (1, '', 'admin', '123456', '38D265D96DFA13B0CD36934B268FBAC0', '2020-01-13 17:33:42', '超级管理员', 0, ' ', 0, '2020-01-10 14:42:06', '2019-04-08 10:52:26', '2019-10-15 15:57:10');
INSERT INTO `base_user` VALUES (2, '', 'szm', '123456', '7D44A1010640B9FD88B9F0EECF5F93D4', '2019-10-20 17:34:50', '开发人员szm', 0, '', 0, '2019-10-17 17:32:48', '2019-07-23 14:10:08', '2019-07-23 14:10:08');

-- ----------------------------
-- Table structure for base_user_role
-- ----------------------------
DROP TABLE IF EXISTS `base_user_role`;
CREATE TABLE `base_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_userid`(`user_id`) USING BTREE,
  INDEX `idx_roleid`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user_role
-- ----------------------------
INSERT INTO `base_user_role` VALUES (5, 2, 3);
INSERT INTO `base_user_role` VALUES (9, 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
